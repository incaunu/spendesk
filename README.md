# Getting Started

## Installation

Clone the repository

```bash
git clone https://incaunu@bitbucket.org/incaunu/spendesk.git
```
Install the dependencies

```bash
npm install
```

## Run

```bash
npm start
```

## Notes

The approval schema validation is not the best implementation.
I could have used a package like `formik` or `redux-form` (as Lucas suggested) but I did not want to spend too much time to figure it out how it works.
Also, I'm not displaying any validation errors for `amount` input.

The `teams` data from server has an error, the last team id duplicated. To fix this, I've removed it.

As a side effect middleware I've used `redux-saga` instead of `thunk`. I'm more familiar with it (and I also like it more than `thunk`).

Not sure if I had to save the approval schema in local storage or add `redux-persist` package. Right now it's only saved in redux and on page refresh the data will be lost.
