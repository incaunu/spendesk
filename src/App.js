import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Container from "@material-ui/core/Container";

// const TeamsPage = React.lazy(() => import('./Teams')); and together with Suspense we can code split our features
import { TeamsPage } from "./Teams";

function App() {
  return (
    <Container fixed>
      <Router>
        <Switch>
          <Route path="/teams" component={TeamsPage} />
          <Redirect to="/teams" />
        </Switch>
      </Router>
    </Container>
  )
}

export default App;
