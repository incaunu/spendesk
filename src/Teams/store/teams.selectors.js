import { TEAMS_STATE_KEY } from "./teams.reducer";
import { createSelector } from "reselect";
import { isApprovalSchemaValid } from "../shared/utils";

const rootSelector = state => state[TEAMS_STATE_KEY];

const teamsSelector = state => rootSelector(state).teams;
const isLoadingTeamsSelector = state => rootSelector(state).isLoadingTeams;
const errorSelector = state => rootSelector(state).error;
const activeTeamSelector = state => rootSelector(state).activeTeam;

const areTeamsLoadedSelector = createSelector(teamsSelector, teams => teams !== null);

const isActiveTeamApprovalSchemaValidSelector = createSelector(activeTeamSelector,
  activeTeam => isApprovalSchemaValid(activeTeam));

export default {
  teams: teamsSelector,
  isLoadingTeams: isLoadingTeamsSelector,
  areTeamsLoaded: areTeamsLoadedSelector,
  error: errorSelector,
  activeTeam: activeTeamSelector,
  isActiveTeamApprovalSchemaValid: isActiveTeamApprovalSchemaValidSelector,
}
