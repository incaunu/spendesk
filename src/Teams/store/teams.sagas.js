// eslint-disable-next-line no-unused-vars
import { takeLatest, all, put, delay, call } from 'redux-saga/effects'
import { ActionTypes, getTeamsSuccessAction, saveApprovalSchemaSuccessAction, getTeamsFailureAction, closeApprovalSchemaAction } from "./teams.actions";

// Used mocked data during development
// import teamsJson from '../../teamsData.json';
// import usersJson from '../../usersData.json';

function* getTeams() {
  try {
    // yield delay(500);

    const teamsResponse = yield call(fetch, 'https://s3-eu-west-1.amazonaws.com/spx-development/contents/teams');
    const usersResponse = yield call(fetch, 'https://s3-eu-west-1.amazonaws.com/spx-development/contents/users');

    const teams = yield teamsResponse.json();
    const users = yield usersResponse.json();

    // Uncomment this line to simulate a request failure
    // throw new Error('Could not load data from server');

    // The `teams` data from server has an error, the last team id duplicated. To fix this, I've removed it.
    teams.pop();

    const teamsWithUsers = teams.map(team => ({
      ...team,
      approvalSchema: [],
      users: team.users.map(userId => {
        const { first_name, last_name, ...restUserProps } = users.find(user => userId === user.id);
        return {
          ...restUserProps,
          firstName: first_name,
          lastName: last_name,
          name: `${first_name} ${last_name}`,
        }
      }),
    }));

    yield put(getTeamsSuccessAction(teamsWithUsers));
  } catch (error) {
    yield put(getTeamsFailureAction(error.toString()));
  }
}

function* saveApprovalSchema() {
  // TODO - save schema on server...
  yield put(saveApprovalSchemaSuccessAction());
  yield put(closeApprovalSchemaAction());
}

function* getTeamsWatcher() {
  yield takeLatest(ActionTypes.GET_TEAMS, getTeams);
}

function* saveApprovalSchemaWatcher() {
  yield takeLatest(ActionTypes.SAVE_APPROVAL_SCHEMA, saveApprovalSchema);
}


export default function*() {
  yield all([getTeamsWatcher(), saveApprovalSchemaWatcher()]);
}
