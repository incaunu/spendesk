import store from '../../store';
import teamsReducer, { TEAMS_STATE_KEY } from "./teams.reducer";
import teamsSagas from './teams.sagas';

export default () => {
  store.injectReducer(TEAMS_STATE_KEY, teamsReducer);
  store.runSaga(teamsSagas);
}
