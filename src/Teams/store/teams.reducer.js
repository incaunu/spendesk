import { ActionTypes } from "./teams.actions";
import { RULE_TYPES } from "../shared/enums";

export const TEAMS_STATE_KEY = 'teams';

const initialState = {
  teams: null,
  isLoadingTeams: false,
  error: null,

  activeTeam: null,
  showApprovalsDialog: false,
  isSavingApprovalSchema: false,
}

function addRule(team) {
  const approvalSchema = [...team.approvalSchema];
  const hasRules = approvalSchema.length !== 0;
  const hasOneRule = approvalSchema.length === 1;
  const lastRule = approvalSchema.length ? approvalSchema[approvalSchema.length - 1] : null;

  const newRule = {
    type: RULE_TYPES.range,
    user: {},
    amount: 0,
  }

  if (lastRule && lastRule.type === RULE_TYPES.above) {
    if (hasOneRule) {
      newRule.type = RULE_TYPES.upTo;
    }
    approvalSchema.splice(approvalSchema.length - 1, 0, newRule);
  } else {
    if (!hasRules) {
      newRule.type = RULE_TYPES.upTo;
    }
    approvalSchema.push(newRule);
  }

  return {
    ...team,
    approvalSchema,
  }
}

function updateRuleType(team, { ruleIndex, type }) {
  const rule = { ...team.approvalSchema[ruleIndex] };
  rule.type = type;

  if (ruleIndex > 0 && type === RULE_TYPES.above) {
    const previousRule = team.approvalSchema[ruleIndex - 1];
    rule.amount = previousRule.amount;
  } else if (ruleIndex === 0 && type === RULE_TYPES.above) {
    rule.amount = 0;
  }

  return updateRule(team, ruleIndex, rule)
}

function updateRuleAmount(team, { ruleIndex, amount }) {
  const rule = { ...team.approvalSchema[ruleIndex] };
  rule.amount = amount;

  if (ruleIndex === team.approvalSchema.length - 2) {
    const nextRule = team.approvalSchema[ruleIndex + 1];
    if (nextRule.type === RULE_TYPES.above) {
      nextRule.amount = amount;
    }
  }

  return updateRule(team, ruleIndex, rule)
}

function updateRuleUser(team, { ruleIndex, userId }) {
  const rule = { ...team.approvalSchema[ruleIndex] };
  rule.user = team.users.find(user => user.id === userId);

  return updateRule(team, ruleIndex, rule)
}

function updateRule(team, ruleIndex, rule) {
  return {
    ...team,
    approvalSchema: team.approvalSchema.map((r, index) => ruleIndex === index ? rule : { ...r }),
  }
}

function deleteRule(team, { index }) {
  const approvalSchema = [...team.approvalSchema];
  const hasOneRule = approvalSchema.length === 1;

  if (!hasOneRule && index === 0) {
    const secondRule = approvalSchema[1];
    if (secondRule.type !== RULE_TYPES.above) {
      secondRule.type = RULE_TYPES.upTo;
    }
  }

  return {
    ...team,
    approvalSchema: team.approvalSchema.filter((_, i) => i !== index),
  }
}


function teamsReducer(state = initialState, { type, payload }) {
  switch (type) {
    case ActionTypes.GET_TEAMS:
      return {
        ...state,
        error: null,
        isLoadingTeams: true,
      }
    case ActionTypes.GET_TEAMS_SUCCESS:
      return {
        ...state,
        isLoadingTeams: false,
        teams: payload.teams,
      }
    case ActionTypes.GET_TEAMS_FAILURE:
      return {
        ...state,
        isLoadingTeams: false,
        error: payload.error,
      }
    case ActionTypes.OPEN_APPROVAL_SCHEMA:
      return {
        ...state,
        activeTeam: payload.team,
      }
    case ActionTypes.CLOSE_APPROVAL_SCHEMA:
      return {
        ...state,
        activeTeam: null,
      }
    case ActionTypes.SAVE_APPROVAL_SCHEMA:
      return {
        ...state,
        isSavingApprovalSchema: true,
      }
    case ActionTypes.SAVE_APPROVAL_SCHEMA_SUCCESS:
      return {
        ...state,
        isSavingApprovalSchema: false,
        teams: state.teams.map(team => team.id === state.activeTeam.id ? state.activeTeam : team),
      }
    case ActionTypes.ADD_RULE:
      return {
        ...state,
        activeTeam: addRule(state.activeTeam),
      }
    case ActionTypes.UPDATE_RULE_TYPE:
      return {
        ...state,
        activeTeam: updateRuleType(state.activeTeam, payload),
      }
    case ActionTypes.UPDATE_RULE_AMOUNT:
      return {
        ...state,
        activeTeam: updateRuleAmount(state.activeTeam, payload),
      }
    case ActionTypes.UPDATE_RULE_USER:
      return {
        ...state,
        activeTeam: updateRuleUser(state.activeTeam, payload),
      }
    case ActionTypes.DELETE_RULE:
      return {
        ...state,
        activeTeam: deleteRule(state.activeTeam, payload),
      }
    default:
      return state;
  }
}

export default teamsReducer;
