// I could have used redux-actions package to define actions, handlers (https://github.com/redux-utilities/redux-actions)

const suffix = '[TEAMS]';

export const ActionTypes = {
  GET_TEAMS: `${suffix} GET_TEAMS`,
  GET_TEAMS_SUCCESS: `${suffix} GET_TEAMS_SUCCESS`,
  GET_TEAMS_FAILURE: `${suffix} GET_TEAMS_FAILURE`,

  OPEN_APPROVAL_SCHEMA: `${suffix} OPEN_APPROVAL_SCHEMA`,
  CLOSE_APPROVAL_SCHEMA: `${suffix} CLOSE_APPROVAL_SCHEMA`,
  SAVE_APPROVAL_SCHEMA: `${suffix} SAVE_APPROVAL_SCHEMA`,
  SAVE_APPROVAL_SCHEMA_SUCCESS: `${suffix} SAVE_APPROVAL_SCHEMA_SUCCESS`,

  ADD_RULE: `${suffix} ADD_RULE`,
  UPDATE_RULE_AMOUNT: `${suffix} UPDATE_RULE_AMOUNT`,
  UPDATE_RULE_USER: `${suffix} UPDATE_RULE_USER`,
  UPDATE_RULE_TYPE: `${suffix} UPDATE_RULE_TYPE`,
  DELETE_RULE: `${suffix} DELETE_RULE`,
}

export const getTeamsAction = () => ({ type: ActionTypes.GET_TEAMS });
export const getTeamsSuccessAction = (teams) => ({
  type: ActionTypes.GET_TEAMS_SUCCESS,
  payload: {
    teams
  },
});
export const getTeamsFailureAction = (error) => ({
  type: ActionTypes.GET_TEAMS_FAILURE,
  payload: {
    error,
  }
});

export const openApprovalSchemaAction = (team) => ({
  type: ActionTypes.OPEN_APPROVAL_SCHEMA,
  payload: {
    team,
  },
});
export const closeApprovalSchemaAction = () => ({
  type: ActionTypes.CLOSE_APPROVAL_SCHEMA,
});
export const saveApprovalSchemaAction = () => ({
  type: ActionTypes.SAVE_APPROVAL_SCHEMA,
});
export const saveApprovalSchemaSuccessAction = () => ({
  type: ActionTypes.SAVE_APPROVAL_SCHEMA_SUCCESS,
});

export const addRuleAction = () => ({
  type: ActionTypes.ADD_RULE,
});
export const updateRuleTypeAction = (ruleIndex, type) => ({
  type: ActionTypes.UPDATE_RULE_TYPE,
  payload: {
    ruleIndex,
    type,
  }
});
export const updateRuleAmountAction = (ruleIndex, amount) => ({
  type: ActionTypes.UPDATE_RULE_AMOUNT,
  payload: {
    ruleIndex,
    amount,
  }
});
export const updateRuleUserAction = (ruleIndex, userId) => ({
  type: ActionTypes.UPDATE_RULE_USER,
  payload: {
    ruleIndex,
    userId,
  }
});
export const deleteRuleAction = (index) => ({
  type: ActionTypes.DELETE_RULE,
  payload: {
    index,
  }
});
