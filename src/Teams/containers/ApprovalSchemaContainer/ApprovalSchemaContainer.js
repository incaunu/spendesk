import React from 'react';
import { connect } from "react-redux";
import { ApprovalSchema } from "../../components";
import {
  saveApprovalSchemaAction,
  closeApprovalSchemaAction,
  addRuleAction,
  updateRuleTypeAction,
  updateRuleAmountAction,
  updateRuleUserAction,
  deleteRuleAction
} from '../../store/teams.actions';
import teamsSelectors from "../../store/teams.selectors";

function ApprovalSchemaContainer({ activeTeam, isApprovalSchemaValid, saveApprovalSchema, closeApprovalSchema, addRule, deleteRule, updateRuleType, updateRuleAmount, updateRuleUser }) {
  return activeTeam && (
    <ApprovalSchema
      team={activeTeam}
      isApprovalSchemaValid={isApprovalSchemaValid}
      onClose={closeApprovalSchema}
      onSave={saveApprovalSchema}
      addRule={addRule}
      deleteRule={deleteRule}
      updateRuleType={updateRuleType}
      updateRuleAmount={updateRuleAmount}
      updateRuleUser={updateRuleUser}
      isSaving={false}
    />
  );
}

const mapDispatchToProps = dispatch => ({
  saveApprovalSchema: () => {
    dispatch(saveApprovalSchemaAction())
  },
  closeApprovalSchema: () => {
    dispatch(closeApprovalSchemaAction())
  },
  addRule: () => {
    dispatch(addRuleAction())
  },
  deleteRule: (ruleIndex) => {
    dispatch(deleteRuleAction(ruleIndex))
  },
  updateRuleType: (ruleIndex, type) => {
    dispatch(updateRuleTypeAction(ruleIndex, type))
  },
  updateRuleAmount: (ruleIndex, amount) => {
    dispatch(updateRuleAmountAction(ruleIndex, amount))
  },
  updateRuleUser: (ruleIndex, user) => {
    dispatch(updateRuleUserAction(ruleIndex, user))
  },
})

const mapStateToProps = state => ({
  activeTeam: teamsSelectors.activeTeam(state),
  isApprovalSchemaValid: teamsSelectors.isActiveTeamApprovalSchemaValid(state),
});

// I also could have used 'useDispatch` and `useSelector` hooks...
export default connect(mapStateToProps, mapDispatchToProps)(ApprovalSchemaContainer);
