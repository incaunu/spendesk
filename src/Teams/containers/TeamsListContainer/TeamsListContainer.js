import React from 'react';
import { connect } from "react-redux";
import { TeamsList } from "../../components";
import teamsSelectors from '../../store/teams.selectors';
import { getTeamsAction, openApprovalSchemaAction } from '../../store/teams.actions';

function TeamsListContainer({ getTeams, teams, openApprovalSchema, isLoadingTeams, error, areTeamsLoaded }) {
  const handleEditTeam = React.useCallback((team) => {
    // deep clone so that we don't modify the original team while editing the approval schema
    openApprovalSchema(JSON.parse(JSON.stringify(team)));
  }, [openApprovalSchema]);

  return (
    <TeamsList getTeams={getTeams} teams={teams} isLoadingTeams={isLoadingTeams} error={error} areTeamsLoaded={areTeamsLoaded} onTeamEdit={handleEditTeam} />
  )
}

const mapDispatchToProps = dispatch => ({
  getTeams: () => {
    dispatch(getTeamsAction())
  },
  openApprovalSchema: (team) => {
    dispatch(openApprovalSchemaAction(team))
  }
})

const mapStateToProps = state => ({
  teams: teamsSelectors.teams(state),
  isLoadingTeams: teamsSelectors.isLoadingTeams(state),
  error: teamsSelectors.error(state),
  areTeamsLoaded: teamsSelectors.areTeamsLoaded(state),
});

// I also could have used 'useDispatch` and `useSelector` hooks...
export default connect(mapStateToProps, mapDispatchToProps)(TeamsListContainer);
