import { RULE_TYPES } from "./enums";

export const isApprovalSchemaValid = (team) => {
  if (team === null) {
    return true;
  }

  const { approvalSchema } = team;

  for (let i = 0; i < approvalSchema.length; i++) {
    const rule = approvalSchema[i];
    if (!rule.user.id) {
      return false;
    }
    if (Number.isNaN(parseInt(rule.amount, 10))) {
      return false;
    }
    if (i === 0) {
      if (rule.type === RULE_TYPES.upTo && rule.amount === 0) {
        return false;
      }
    } else {
      const previousRule = approvalSchema[i - 1];
      if (rule.type !== RULE_TYPES.above && previousRule.amount >= rule.amount) {
        return false;
      }
    }
  }

  return true;
}
