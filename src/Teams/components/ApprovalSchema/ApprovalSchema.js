import React from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import ApprovalSchemaRule from "../ApprovalSchemaRule/ApprovalSchemaRule";
import Typography from "@material-ui/core/Typography";
import { RULE_TYPES } from "../../shared/enums";

function ApprovalSchema({ team, isApprovalSchemaValid, onClose, onSave, addRule, deleteRule, updateRuleType, updateRuleAmount, updateRuleUser }) {
  const { approvalSchema, users } = team;

  const hasRules = React.useMemo(() => approvalSchema.length > 0, [approvalSchema]);
  const lastRule = React.useMemo(() => hasRules && approvalSchema[approvalSchema.length - 1], [hasRules, approvalSchema]);
  const canAddRule = React.useMemo(() => approvalSchema.length < users.length, [users, approvalSchema]);

  const rules = React.useMemo(() => {
    if (hasRules && lastRule.type === RULE_TYPES.above) {
      return {
        firstRules: approvalSchema.slice(0, -1),
        lastRule,
      }
    }
    return {
      firstRules: approvalSchema,
    };
  }, [approvalSchema, hasRules, lastRule]);

  return (
    <Dialog open onClose={onClose} disableBackdropClick disableEscapeKeyDown maxWidth="sm" fullWidth>
      <DialogTitle disableTypography>
        <Typography variant="h6">
          Set up approvers
        </Typography>
        <Typography variant="body2">
          Who can approve requests of the team {team.name}?
        </Typography>
      </DialogTitle>
      <DialogContent>
        {canAddRule && !hasRules && (
          <Typography variant="body1" align="center">
            You don't have any rules, yet.
          </Typography>
        )}
        {!canAddRule && !hasRules && (
          <Typography variant="body1">
            It looks like you can't add any rule because there are no users assigned to this team
          </Typography>
        )}

        {rules.firstRules.map((_, index) => (
          <ApprovalSchemaRule
            key={index}
            team={team}
            ruleIndex={index}
            deleteRule={deleteRule}
            updateRuleType={updateRuleType}
            updateRuleAmount={updateRuleAmount}
            updateRuleUser={updateRuleUser}
          />
        ))}
        {canAddRule && (
          <Button fullWidth onClick={addRule} variant="outlined">Add a new rule</Button>
        )}
        {rules.lastRule && (
          <ApprovalSchemaRule
            team={team}
            ruleIndex={approvalSchema.length - 1}
            deleteRule={deleteRule}
            updateRuleType={updateRuleType}
            updateRuleAmount={updateRuleAmount}
            updateRuleUser={updateRuleUser}
          />
        )}
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>Cancel</Button>
        <Button
          onClick={onSave}
          variant="contained"
          color="primary"
          disableElevation
          disabled={!isApprovalSchemaValid}
        >
          Save
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default ApprovalSchema;
