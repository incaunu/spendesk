import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import { RULE_TYPES } from "../../shared/enums";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import TextField from "@material-ui/core/TextField";
import FormHelperText from "@material-ui/core/FormHelperText";
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(1),
    margin: [[theme.spacing(1), 0]],
    border: '1px dashed grey'
  },
  formControl: {
    marginLeft: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

function ApprovalSchemaRule({ team, ruleIndex, deleteRule, updateRuleType, updateRuleAmount, updateRuleUser }) {
  const classes = useStyles();
  const { approvalSchema, users } = team;
  const rule = approvalSchema[ruleIndex];

  const availableUsers = React.useMemo(() => {
    return users.filter((user) => {
        return !approvalSchema.find((rule, index) => {
          return ruleIndex !== index && rule.user.id === user.id
        })
      }
    );
  }, [users, approvalSchema, ruleIndex]);

  const handleRuleTypeChange = (event) => {
    updateRuleType(ruleIndex, event.target.value);
  }
  const handleAmountChange = (event) => {
    let amount = parseInt(event.target.value, 10);
    if (Number.isNaN(amount)) {
      amount = 0;
    }
    updateRuleAmount(ruleIndex, amount);
  }

  const handleUserChange = (event) => {
    updateRuleUser(ruleIndex, event.target.value);
  };

  const handleDeleteRule = () => {
    deleteRule(ruleIndex);
  };

  const renderRuleConditions = () => {
    if (ruleIndex === 0) {
      const hasOneRule = approvalSchema.length === 1;
      return (
        <>
          <RadioGroup value={rule.type} onChange={handleRuleTypeChange} row>
            <FormControlLabel value={RULE_TYPES.upTo} control={<Radio size="small" />} label="Up to" />
            {hasOneRule && <FormControlLabel value={RULE_TYPES.above} control={<Radio size="small" />} label="Any amount" />}
          </RadioGroup>
          {rule.type === RULE_TYPES.upTo && <TextField InputProps={{ type: 'number' }} label="Amount" value={rule.amount} onChange={handleAmountChange}/>}
        </>
      )
    }

    const isLastRule = ruleIndex === approvalSchema.length - 1;
    return (
      <>
        <RadioGroup value={rule.type} onChange={handleRuleTypeChange} row>
          <FormControlLabel value={RULE_TYPES.range} control={<Radio size="small" />} label="Range" />
          {isLastRule && <FormControlLabel value={RULE_TYPES.above} control={<Radio size="small" />} label="Above" />}
        </RadioGroup>
        {rule.type === RULE_TYPES.range && (
          <>
            From {approvalSchema[ruleIndex - 1].amount} to &nbsp;&nbsp;
            <TextField InputProps={{ type: 'number' }} label="Amount" value={rule.amount} onChange={handleAmountChange}/>
          </>
        )}
        {rule.type === RULE_TYPES.above && (
          <TextField InputProps={{ readOnly: true, type: 'number' }} label="Amount" value={rule.amount} />
        )}
      </>
    )
  }

  return (
    <div className={classes.root}>
      {renderRuleConditions()}
      <FormControl className={classes.formControl}>
        <InputLabel>Approver</InputLabel>
        <Select
          error={!rule.user.id}
          value={rule.user.id ? rule.user.id : ''}
          onChange={handleUserChange}
        >
          {availableUsers.map((user) => (
            <MenuItem key={user.id} value={user.id}>{user.name}</MenuItem>
          ))}
        </Select>
        {!rule.user.id && <FormHelperText>No approver selected</FormHelperText>}
      </FormControl>
      <IconButton onClick={handleDeleteRule}>
        <DeleteIcon />
      </IconButton>
    </div>
  )
}

export default ApprovalSchemaRule;
