import React from 'react';
import TeamCard from "../TeamCard/TeamCard";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

function TeamsList({ teams, onTeamEdit, isLoadingTeams, error, areTeamsLoaded, getTeams }) {
  React.useEffect(() => {
    getTeams();
  }, [getTeams])

  if (error) {
    return (
      <>
        <div>Error loading teams {error}</div>
        <Button onClick={getTeams} color="primary">Retry</Button>
      </>
    )
  }
  if (!areTeamsLoaded || isLoadingTeams) {
    return <div>Loading teams</div>
  }

  return (
    <Grid container spacing={2}>
      {teams.map(team => (
        <Grid key={team.id} item sm={12} md={6}>
          <TeamCard team={team} onEdit={onTeamEdit} />
        </Grid>
      ))}
    </Grid>
  )
}

export default TeamsList;
