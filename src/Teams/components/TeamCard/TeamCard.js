import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Chip from "@material-ui/core/Chip";
import CardHeader from "@material-ui/core/CardHeader";

const useStyles = makeStyles((theme) => ({
  chipsContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      marginRight: theme.spacing(0.5),
      marginBottom: theme.spacing(0.5),
    },
  },
}));

function TeamCard({ team, onEdit }) {
  const classes = useStyles();

  const handleEdit = React.useCallback(() => {
    onEdit(team);
  }, [onEdit, team]);

  const firstThreeUsers = React.useMemo(() => team.users.slice(0, 3), [team]);
  const firstThreeApprovers = React.useMemo(() => team.approvalSchema.slice(0, 3), [team]);

  return (
    <Card>
      <CardHeader title={team.name} />
      <CardContent>

        <Typography variant="body2">
          Top {firstThreeUsers.length} users
        </Typography>
        <div className={classes.chipsContainer}>
          {firstThreeUsers.map(user => (
            <Chip key={user.id} size="small" label={user.name} variant="outlined" />
          ))}
        </div>

        {!firstThreeApprovers.length && (
          <Typography variant="body2">
            There is no approval schema defined
          </Typography>
        )}

        {firstThreeApprovers.length > 0 && (
          <>
            <Typography variant="body2">
              Top {firstThreeApprovers.length} approvers
            </Typography>
            <div className={classes.chipsContainer}>
              {firstThreeApprovers.map(rule => (
                <Chip key={rule.user.id} size="small" label={rule.user.name} variant="outlined" />
              ))}
            </div>
          </>
        )}
      </CardContent>
      <CardActions>
        <Button onClick={handleEdit}>
          Edit approvals
        </Button>
      </CardActions>
    </Card>
  )
}

export default TeamCard;
