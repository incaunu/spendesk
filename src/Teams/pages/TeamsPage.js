import React from 'react';
import loadFeatureStore from '../store/teams.store';
import { TeamsListContainer, ApprovalSchemaContainer } from '../containers'

loadFeatureStore();

function TeamsPage() {
  return (
    <>
      <TeamsListContainer />
      <ApprovalSchemaContainer />
    </>
  )
}

export default TeamsPage;
