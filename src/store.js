import { createStore, applyMiddleware, combineReducers } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension'

const sagaMiddleware = createSagaMiddleware()

// I also wanted to display the currency in the approval schema for a specific customer...
const rootReducers = {
  user: () => {
    return {
      id: 0,
      name: 'Moldovean Radu-Cristian',
      company: 'mine',
      currency: 'Eur',
      symbol: '€',
    }
  }
}

const asyncReducers = {};

function createReducer(asyncReducers = {}) {
  return combineReducers({
    ...rootReducers,
    ...asyncReducers
  })
}

const middlewares = [sagaMiddleware];
const middlewareEnhancer = applyMiddleware(...middlewares)

const enhancers = [middlewareEnhancer]
const composedEnhancers = composeWithDevTools(...enhancers)

const store = createStore(createReducer(), composedEnhancers);

// This is a simulation on how to lazy load reducers/sagas
// A more complex package that I could have used is: https://github.com/microsoft/redux-dynamic-modules

store.runSaga = sagaMiddleware.run;
store.injectReducer = (key, asyncReducer) => {
  asyncReducers[key] = asyncReducer
  store.replaceReducer(createReducer(asyncReducers))
}

export default store;
